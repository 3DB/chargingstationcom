#include <Arduino.h>
#include <time.h>

// const define
#define CP_READ_ITERATIONS 100  // How often CP should be read
#define PP_R1 1000 // R1 in PP equation
#define PP_Uges 5 // Uges in PP equation
#define DIODE_TEST false

#define CP_READ PC2  // Control pilot read (Analog)
#define PP_READ PC1  // Proximity pilot read (Analog)
#define RELAY 8  // Control solid-state-relay (Digital)
#define CP_CONTROL PB2  // PWM signal control pilot (Digital)
#define READ_AUTHENTICATED PC0    // Signals whether user is authenticated (Digital)
#define DIGITAL_OUT2 PD3
#define DIGITAL_OUT1 PD4
#define DIGITAL_IN1 PD6
#define DIGITAL_IN2 PD7

// debug defines
#define MAIN true
#define CP_READ true
#define VOLTAGES true
#define INIT true
#define CP_WRITE true
#define CHARGING true
#define PP_READ true
#define VOLTAGES_CP false

enum State {STANDBY = 0, VEHICLE_DETECTED = 1, READY = 2, WITH_VENTILATION = 3, NO_POWER = 4, FAILED = 5};
const String FIRMWARE_VERSION = "0.3";  // Current firmware version

// Globals
time_t g_begin_charge_time;
time_t g_end_charging_time;
enum State g_current_state;
bool g_authenticated;
int g_failed_waiting_time = 0;
int g_failed_counter_start_time;
long g_last_charging_millis;
float g_charged_power;
bool g_is_cp_pwm_active;
bool charging = false;

int cpPwmByCurrent(int maxAmp);
void cpWrite(bool b_enable_pwm);
void disableCharging();

void setup() {
    // serial for Debug log
    // Set serial baud rate
    Serial.begin(9600);

    // Startup message
    String str = "Starting up ChargingStation v" + FIRMWARE_VERSION + "...";
    println(str, INIT)

    // Init pins
    println("Setting up PINS...", INIT)
    pinMode(CP_READ, INPUT);
    pinMode(PP_READ, INPUT);
    pinMode(RELAY, OUTPUT);
    pinMode(CP_CONTROL, OUTPUT);
    pinMode(READ_AUTHENTICATED, INPUT);
    pinMode(DIGITAL_OUT1, OUTPUT);

    // Setup for pwm
    println("Setting up PWM...", INIT)
    pinMode(PB3, OUTPUT);
    pinMode(PB4, OUTPUT);
    TCCR1A = 0;
    TCCR1B = 0;

    // enableCharging();

    // set State
    println("State: STANDBY", INIT)
    g_current_state = STANDBY;
}

void loop() {
    State newState = cpRead();

    bool bStateChanged = false;
    if (newState != g_current_state) {
        print("Switched to: ", MAIN)
        g_current_state = newState;
        bStateChanged = true;
    }

    switch(newState) {
        case STANDBY:
            println("STANDBY", MAIN && bStateChanged);
            disableCharging();
            cpWrite(false);
            break;

        case VEHICLE_DETECTED:
            println("VEHICLE_DETECTED", MAIN && bStateChanged);
            if (isAuthenticated()) {
                disableCharging();
                cpWrite(true);
            }
            break;

        case WITH_VENTILATION:
            println("WITH_VENTILATION is going to be treated as: ", MAIN && bStateChanged);
        
        case FAILED:
            println("FAILED", MAIN && bStateChanged);
            disableCharging();
            cpWrite(false);
            break;
        
        case READY:
            println("READY", MAIN && bStateChanged);
            if (isAuthenticated) {
                enableCharging();
                cpWrite(true);
            }
            break;

        case NO_POWER:
            println("NO POWER", MAIN && bStateChanged);
            disableCharging();
            cpWrite(false);
            break;
    }
}

/**
 * Returns ampacity for the cable. Returns -1 if error.
 */
int ppCheck() {
    int u2 = analogRead(PP_READ);
    int r2 = (u2 * PP_R1) / (5 - u2);
    int maxAmp = -1;

    println(String(u2) + "V", PP_READ);
    println(String(r2) + "Ohm", PP_READ);

    if (r2 >= 1000 && r2 < 2000)
        maxAmp = 13;

    else if (r2 >= 330 && r2 < 1000)
        maxAmp = 20;

    else if (r2 >= 150 && r2 < 330)
        maxAmp = 32;

    else if (r2 >= 75 && r2 < 150)
        maxAmp = 63;

    println(String(maxAmp) + "A is our maximum current", PP_READ);

    return maxAmp;
}

/**
 * Opens the charging relay and logs the beginning time of charging.
 */
void enableCharging() {
    if (!isCharging()) {
        println("Begin charging...", CHARGING);
        charging = true;
        digitalWrite(RELAY, HIGH);
        g_last_charging_millis = millis();
    }
}

/**
 * Closes the charging relay if it's currently open and logs time of charging end.
 */
void disableCharging() {
    digitalWrite(RELAY, LOW);
    if (isCharging()) {
        charging = false;
        println("Charging finished!", CHARGING);
    }
}

/**
 * Writes the PWM signal containing the maximum possible charging ampacity.
 */
void cpWrite(bool b_enable_pwm) {
    if (b_enable_pwm) {
        println("CP writing started", CP_WRITE);
        TCCR1A |= (1<<WGM11) | (1<<COM1A1) | (1<<COM1B1);
        TCCR1B |= (1<<CS11) | (1<<CS10) | (1<<WGM12) | (1<<WGM13);
        OCR1A = 100;
        ICR1 = 249;
        OCR1A = (cpPwmByCurrent(20) * 0.01) * 249; // Duty Cycl
    } else {
        print("CP writing stopped", CP_WRITE);
        TCCR2A = 0;
        TCCR2B = 0;
        OCR2A = 0;
        OCR2B = 0;
    }
}



/**
 * Reads CP's voltage and returns the corresponding state.
 */
enum State cpRead() {
    int voltage;
    int pLow = 1023;
    int pHigh = 0;
    enum State newState = FAILED;

    // 0V/0 = -12V; 1023/5V = 12V
    for (int i = 0; i < CP_READ_ITERATIONS; i++) {
      voltage = analogRead(CP_READ);
      if (VOLTAGES_CP)
        println("Voltage: " + voltage + "V", VOLTAGES);
      if (voltage > pHigh)
        pHigh = voltage;
      else if (voltage < pLow)
        pLow = voltage;
    }

    if (pHigh < 962 && pHigh > 899)
        newState = STANDBY;

    else if (pHigh < 868 && pHigh > 806)
        newState = VEHICLE_DETECTED;

    else if (pHigh < 775 && pHigh > 712)
        newState = READY;

    else if (pHigh < 681 && pHigh > 619)
        newState = WITH_VENTILATION;

    else if (pHigh < 588 && pHigh > 525)
        newState = NO_POWER;


    if (!(pLow < 214 && pLow > 151) && g_is_cp_pwm_active && DIODE_TEST){
        newState = FAILED;
        print("Diode Test failed!", CP_READ);
    }

    // Return new state
    return newState;
}

/**
 * Calculates the PWM signal's duty cycle by maximum possible charging ampacity.
 */
int cpPwmByCurrent(int maxAmp) {
    if (maxAmp < 6)
        return 10;

    else if (maxAmp > 48)
        return 70;

    return maxAmp / 0.6;
}

/**
* Checks whether user is authenticated.
*/
bool isAuthenticated() {
   return analogRead(READ_AUTHENTICATED) > 800;
}

/**
 * Returns true if on one phase is more load than 1 Amp, else false.
 */
bool isCharging() {
    return charging;
}

void println(String text, bool pd) {
    if (pd) {
        print(text + "\n", pd)
    }
}

void print(String text, bool pd) {
    if (pd) {
        Serial.printf("%s", text);
    }
}
